import { useState, useEffect } from 'react'
import eventsData from '../../data/events'

const useController = () => {
  const [events, setEvents] = useState([])
  const [loading, setLoading] = useState(false)
  const [error, setError] = useState(null)

  useEffect(() => {
    const fetchEvents = async () => {
      setLoading(true)
      try {
        eventsData.sort((a, b) => a.start.localeCompare(b.start))
        /**   with moment js 
           eventsData.sort((a, b) => moment(a.start, 'HH:mm').diff(moment(b.start, 'HH:mm')));
            const itemStart = moment(item.start, 'HH:mm')
            const itemEnd = itemStart.clone().add(item.duration, 'minutes')
            const rowEventStart = moment(rowEvent.start, 'HH:mm')
            const rowEventEnd = rowEventStart
              .clone()
              .add(rowEvent.duration, 'minutes')

            if (
              itemStart.isBefore(rowEventEnd) &&
              itemEnd.isAfter(rowEventStart)
            ) 
          }) */
        const newRows = []
        let currentRow = []

        eventsData.forEach((item) => {
          let overlappingFound = false

          currentRow.forEach((rowEvent) => {
            if (
              item.start < rowEvent.start + rowEvent.duration &&
              item.start + item.duration > rowEvent.start
            ) {
              overlappingFound = true
            }
          })

          if (overlappingFound) {
            newRows.push(currentRow)
            currentRow = []
          }

          currentRow.push(item)
        })

        if (currentRow.length > 0) {
          newRows.push(currentRow)
        }

        setEvents(newRows)
      } catch (error) {
        setError(error.message)
      } finally {
        setLoading(false)
      }
    }

    fetchEvents()
  }, [])

  return { events, loading, error }
}

export default useController
